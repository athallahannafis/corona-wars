extends Node2D

var isFullScreen = false

# Called when the node enters the scene tree for the first time.
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

func getInput():
	if Input.is_key_pressed(KEY_F12):
		print("FULLSCREEN")
		ProjectSettings.set_setting("display/window/size/fullscreen", true)
		ProjectSettings.set_setting("display/window/size/borderless", true)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	getInput()
	position = get_global_mouse_position()
