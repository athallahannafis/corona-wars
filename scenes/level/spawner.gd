extends StaticBody2D

export (int) var hitPoints = 10
var enemy
var enemies
var enemyCount

func _ready():
	enemy = preload("res://scenes/enemies/enemy.tscn")

func receiveDamage(damage):
	self.hitPoints -= damage
	if self.hitPoints <= 0:
		self.queue_free()

func _on_timer_timeout():
	var enemyInstance = enemy.instance()
	enemies = get_parent().get_node(".").get_tree().get_nodes_in_group("enemy")
	enemyCount = enemies.size()
	var spawnPoint = get_node("spawnerPoint")
	if spawnPoint != null and enemyCount <= 10:
		enemyInstance.position = spawnPoint.get_global_position()
		get_node("../.").add_child(enemyInstance)

func _on_activateSpawn_body_entered(body):
	if body.is_in_group("player"):
		get_node("timer").start()
		get_node("timer").autostart = true


func _on_activateSpawn_body_exited(body):
	if body.is_in_group("player"):
		get_node("timer").stop()
		get_node("timer").autostart = false

