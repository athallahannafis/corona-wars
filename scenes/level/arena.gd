extends Node2D

export (String) var sceneName = "Arena"
export (String) var nextLevel = "level2_arena"
var levelPath = "res://scenes/level/"
var transitionScene = "res://scenes/Transition.tscn"

func _ready():
	MainMenuBgm_Node.get_node("MainMenuBGM").stop()

func _process(delta):
	DepartureBgm_Node.get_node(".").position = $player.position
	
	var hives = get_tree().get_nodes_in_group("spawner").size()
	if hives == 0:
#		get_tree().change_scene("res://scenes/menu/MainMenu.tscn")
		get_node("CompleteText").visible = true
		yield(get_tree().create_timer(3),"timeout")
		var nextLevelScene = levelPath + nextLevel + ".tscn"
		SceneSwitcher.changeScene(transitionScene, {"nextLevel": nextLevelScene})
