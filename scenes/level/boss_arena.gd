extends Node2D

export (String) var sceneName = "Arena"
export (String) var nextLevel = "level2_arena"
var levelPath = "res://scenes/level/"
var transitionScene = "res://scenes/Transition.tscn"
onready var bossText = get_node("bossText")
onready var playerText = get_node("playerText")
onready var player = get_node("player")
onready var boss = get_node("boss_enemy")
var BOSS_DIALOGUE = [
	"So you have come..",
	"I see you have killed my 'armies'",
	"Hoho.. Why are you so sure about that?",
	"I HAVE INFECTED AND KILLED MILLION PEOPLE IN THIS WORLD",
	"You? Kill me? Many researches' vaccines have failed to kill me",
	"You are no different from them"
	]
var PLAYER_DIALOGUE = [
	"I am here to exterminate you and cure this person",
	"You shall not prevail, I will put an end to this pandemic!",
	"Once I succeeded this one, they will make millions OF ME!",
	"WE'LL SEE ABOUT THAT!"
	]


func dialogue():
	DepartureBgm_Node.get_node("AudioStreamPlayer2D").stop()
	BossFightBgm_Node.get_node("bgm").stop()
	IntroBossBgm_Node.get_node(".").position = get_node("player").position
	IntroBossBgm_Node.get_node("bgm").play()
	playerText.visible = false
	bossText.text = BOSS_DIALOGUE[0]
	yield(get_tree().create_timer(3),"timeout")
	bossText.text = BOSS_DIALOGUE[1]
	yield(get_tree().create_timer(3),"timeout")

	bossText.visible = false
	playerText.visible = true
	playerText.text = PLAYER_DIALOGUE[0]
	yield(get_tree().create_timer(3),"timeout")

	bossText.visible = true
	playerText.visible = false
	bossText.text = BOSS_DIALOGUE[2]
	yield(get_tree().create_timer(3),"timeout")
	bossText.text = BOSS_DIALOGUE[3]
	yield(get_tree().create_timer(3),"timeout")

	bossText.visible = false
	playerText.visible = true
	playerText.text = PLAYER_DIALOGUE[1]
	yield(get_tree().create_timer(3),"timeout")
	playerText.text = PLAYER_DIALOGUE[2]
	yield(get_tree().create_timer(3),"timeout")

	bossText.visible = true
	playerText.visible = false
	bossText.text = BOSS_DIALOGUE[4]
	yield(get_tree().create_timer(3),"timeout")
	bossText.text = BOSS_DIALOGUE[5]
	yield(get_tree().create_timer(3),"timeout")

	bossText.visible = false
	playerText.visible = true
	playerText.text = PLAYER_DIALOGUE[3]
	yield(get_tree().create_timer(3),"timeout")
	playerText.visible = false
	IntroBossBgm_Node.get_node("bgm").stop()
	initiateBattle()
	
func initiateBattle():
	BossFightBgm_Node.get_node("bgm").play()
	player.bossBattleStart = true
	boss.bossBattleStart = true
	
	boss.get_node("greenSpawnerTimer").start()
	boss.get_node("greenSpawnerTimer").autostart = true
	boss.get_node("purpleSpawnerTimer").start()
	boss.get_node("purpleSpawnerTimer").autostart = true
	boss.canFireMiddle = true

func _ready():
	get_node("player/Camera2D").zoom.x = 3
	get_node("player/Camera2D").zoom.y = 3
	dialogue()

func _process(delta):
	BossFightBgm_Node.get_node(".").position = get_node("player").position
	if get_node("boss_enemy") == null:
		yield(get_tree().create_timer(3),"timeout")
		get_tree().change_scene("res://scenes/menu/EndScene.tscn")
	
