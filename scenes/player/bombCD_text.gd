extends Label

var bombCD

func _ready():
	bombCD = get_node("../../../BombCooldown")

func _process(delta):
	self.ALIGN_CENTER
	self.text = str(bombCD.time_left).split(".")[0]
