extends Label

func _process(delta):
	var hitPoint = get_parent().get_parent().get_parent().get_node(".").hitPoint
	self.text = "HP: " + str(hitPoint)
