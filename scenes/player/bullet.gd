extends RigidBody2D

var damage = 1
var bulletImpact

func _ready():
	bulletImpact = preload("res://scenes/player/bulletImpact.tscn")

func _on_DamageArea_body_entered(body):
	if (body.is_in_group("enemyBullet") or body.is_in_group("playerBullet")):
		pass
	if (body.is_in_group("enemy") or body.is_in_group("spawner")):
		body.receiveDamage(damage)
	var impactInstance = bulletImpact.instance()
	impactInstance.position = get_global_position()
	impactInstance.rotation_degrees = rotation_degrees
	get_parent().add_child(impactInstance)
	queue_free()
