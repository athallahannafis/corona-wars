extends StaticBody2D

var damage = 10

func _on_AreaDamage_body_entered(body):
	if body.is_in_group("enemy") or body.is_in_group("spawner"):
		body.receiveDamage(damage)


func _on_explosion_animation_finished():
	self.queue_free()
