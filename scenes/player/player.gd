extends KinematicBody2D

export (int) var speed = 400
export (int) var bulletSpeed = 1500
export (int) var hitPoint = 5

var canDamage = true
var invisDuration = 3
var canFire = true
var canBomb = true
var isSilenced = false
var spreadFireBuff = false
var canSpreadFireBuff = true
var isShaking = false
var bossBattleStart = false
var dead = false
var velocity = Vector2()

var bullet
var bomb

func _ready():
	self.modulate.a = 1.7
	bullet = preload("res://scenes/player/bullet.tscn")
	bomb = preload("res://scenes/player/bomb.tscn")
	
func cameraShake():
	$Camera2D.set_offset(Vector2( \
		rand_range(-1.0, 1.0) * 20, \
		rand_range(-1.0, 1.0) * 20 \
	))

func getInput():
	velocity.x = 0
	velocity.y = 0
	if Input.is_action_pressed("fire") and self.canFire and !self.isSilenced:
		if self.spreadFireBuff:
			fireType2()
		else:
			fireType1()
		self.canFire = false
		$FireRate.start()

	if Input.is_action_pressed("Bomb") and self.canBomb:
		fireBomb()
		bombCooldown()
	
	if Input.is_action_pressed("SpreadFireBuff") and self.canSpreadFireBuff:
		spreadFireCooldown()
		
	if Input.is_action_pressed("moveUp"):
		velocity += Vector2(0,-1)
	if Input.is_action_pressed("moveDown"):
		velocity += Vector2(0,1)
	if Input.is_action_pressed("moveLeft"):
		velocity += Vector2(-1,0)
	if Input.is_action_pressed("moveRight"):
		velocity += Vector2(1,0)
	move_and_slide(velocity * self.speed)

func _physics_process(delta):
	if get_parent().sceneName == "boss_arena":
		if self.hitPoint != 0 and self.bossBattleStart:
			getInput()
	else:
		if self.hitPoint != 0:
			getInput()
	if isShaking:
		cameraShake()
	look_at(get_global_mouse_position())
	

func createInstance(scene):
	var sceneInstance = scene.instance()
	sceneInstance.position = $bulletPoint.get_global_position()
	sceneInstance.rotation_degrees = rotation_degrees
	sceneInstance.apply_impulse(Vector2(), Vector2(self.bulletSpeed, 0).rotated(rotation))
	return sceneInstance

func fireType1():
	$fireSound.play()
	var bulletInstance = createInstance(bullet)
	get_tree().get_root().add_child(bulletInstance)

func fireType2():
	$fireSound.play()
	var bulletInstanceLeft = createInstance(bullet)
	var bulletInstanceRight = createInstance(bullet)
	var bulletInstanceCenter = createInstance(bullet)
	
	bulletInstanceLeft.position = $spreadLeft.get_global_position()
	bulletInstanceLeft.rotation_degrees = rotation_degrees - 10
	bulletInstanceLeft.apply_impulse(Vector2(), Vector2(self.bulletSpeed, 0).rotated(rotation + 50))
	
	bulletInstanceRight.position = $spreadRight.get_global_position()
	bulletInstanceRight.rotation_degrees = rotation_degrees + 10
	bulletInstanceRight.apply_impulse(Vector2(), Vector2(self.bulletSpeed, 0).rotated(rotation - 50))
	
	bulletInstanceCenter.position = $bulletPoint.get_global_position()
	bulletInstanceCenter.rotation_degrees = rotation_degrees
	bulletInstanceCenter.apply_impulse(Vector2(), Vector2(self.bulletSpeed, 0).rotated(rotation))
	
	get_tree().get_root().add_child(bulletInstanceLeft)
	get_tree().get_root().add_child(bulletInstanceRight)
	get_tree().get_root().add_child(bulletInstanceCenter)

func fireBomb():
	self.isShaking = true
	$CameraShakeDuration.start()
	var bombInstance = createInstance(bomb)
	get_tree().get_root().add_child(bombInstance)

func receiveDamage(damage):
	if self.canDamage and !self.dead:
		$playerHitSound.play()
		if self.hitPoint != 0:
			self.hitPoint -= damage
			onInvis()
		if self.hitPoint <= 0:
			print("Player dead")
			self.dead = true
			self.visible = false
			get_node("CanvasLayer/Retry").visible = true
	
func bombCooldown():
	self.canBomb = false
	get_node("CanvasLayer/PlayerGUI/bombCD_text").visible = true
	get_node("CanvasLayer/PlayerGUI/bomb_GUI").modulate.a = 0.3
	$BombCooldown.start()

func _on_BombCooldown_timeout():
	self.canBomb = true
	get_node("CanvasLayer/PlayerGUI/bombCD_text").visible = false
	get_node("CanvasLayer/PlayerGUI/bomb_GUI").modulate.a = 1
	$BombCooldown.stop()

func _on_FireRate_timeout():
	self.canFire = true
	$FireRate.stop()

func onInvis():
	self.canDamage = false
	self.modulate.a = 0.3
	get_node("CanvasLayer/PlayerGUI/hit_screen").visible = true
	$InvisDuration.start()

func _on_InvisDuration_timeout():
	self.canDamage = true
	self.modulate.a = 1.7
	get_node("CanvasLayer/PlayerGUI/hit_screen").visible = false
	$InvisDuration.stop()

func _on_RetryButton_pressed():
	var sceneName = get_parent().sceneName
	get_tree().change_scene(str("res://scenes/level/" + sceneName + ".tscn"))

func _on_MainMenu_pressed():
	get_tree().change_scene("res://scenes/menu/MainMenu.tscn")

func spreadFireCooldown():
	self.spreadFireBuff = true
	self.canSpreadFireBuff = false
	get_node("CanvasLayer/PlayerGUI/fireSpread_GUI").modulate.a = 0.3
	get_node("CanvasLayer/PlayerGUI/fireSpreadCD_text").visible = true
	$SpreadFireDuration.start()
	$SpreadFireCooldown.start()

func _on_SpreadFireDuration_timeout():
	self.spreadFireBuff = false
	$SpreadFireDuration.stop()

func _on_SpreadFireCooldown_timeout():
	self.canSpreadFireBuff = true
	get_node("CanvasLayer/PlayerGUI/fireSpread_GUI").modulate.a = 1
	get_node("CanvasLayer/PlayerGUI/fireSpreadCD_text").visible = false
	$SpreadFireCooldown.stop()


func _on_CameraShakeDuration_timeout():
	self.isShaking = false
