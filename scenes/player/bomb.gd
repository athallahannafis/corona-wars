extends RigidBody2D


func _on_DamageArea_body_entered(body):
	if (body.is_in_group("enemyBullet")) or (body.is_in_group("playerBullet"))\
	or body.is_in_group("player"):
		pass
	else:
		explosionAnimation()
		self.queue_free()

func explosionAnimation():
	var explosion = preload("res://scenes/player/bombImpact.tscn")
	var explosionInstance = explosion.instance()
	explosionInstance.position = $ExplosionPoint.get_global_position()
	explosionInstance.rotation_degrees = self.rotation_degrees
	get_parent().add_child(explosionInstance)
