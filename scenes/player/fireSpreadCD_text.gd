extends Label

var cd

func _ready():
	cd = get_node("../../../SpreadFireCooldown")

func _process(delta):
	self.ALIGN_CENTER
	self.text = str(cd.time_left).split(".")[0]
