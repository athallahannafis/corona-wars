extends Node

var data = null

func changeScene(sceneName, newData):
	data = newData
	get_tree().change_scene(sceneName)
	
func getData(name):
	if data != null and data.has(name):
		return data[name]
	return null
