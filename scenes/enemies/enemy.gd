extends KinematicBody2D

export (int) var hitPoints = 3
var bodyDamage = 1
var fireRate = 1
var bulletSpeed = 600
var canFire = true
var justSpawned = true
var speed = 100
var velocity = Vector2()

var player

func _ready():
	player = get_parent().get_node("player")
	
func _process(delta):
	if self.justSpawned:
		fireCooldown()
		self.justSpawned = false

func _physics_process(delta):
	move1()
	if self.canFire:
		fire()
		
func move1():
	if player != null:
		look_at(player.position)
		self.position += ((player.position - self.position) / self.speed)
		move_and_collide(self.velocity)

func _on_Area2D_body_entered(body):
	if body.is_in_group("player"):
		body.receiveDamage(self.bodyDamage)
	else:
		pass

func receiveDamage(damage):
	self.hitPoints -= damage
	if self.hitPoints <= 0:
		$deathSound.play()
		yield(get_tree().create_timer(0.1),"timeout")
		queue_free()

func fire():
	var bullet = preload("res://scenes/enemies/enemyBullet.tscn")
	var bulletInstance = bullet.instance()
	bulletInstance.position = $bulletPoint.get_global_position()
	bulletInstance.rotation_degrees = rotation_degrees
	bulletInstance.apply_impulse(Vector2(), Vector2(bulletInstance.speed, 0).rotated(rotation))
	get_tree().get_root().add_child(bulletInstance)
	fireCooldown()

func fireCooldown():
	self.canFire = false
	$FireRate.start()

func _on_FireRate_timeout():
	self.canFire = true
	$FireRate.stop()
