extends KinematicBody2D

var velocity = Vector2()
export (int) var hitPoint = 1500
var bodyDamage = 3
var canFireMiddle = false
var canRapid = true
var bulletSpeed = 1500
var bossBattleStart = false

var player
var bullet
var greenEnemy
var purpleEnemy
var deathAnimation

func _ready():
	player = get_parent().get_node("player")
	bullet = preload("res://scenes/enemies/enemyBullet.tscn")
	greenEnemy = preload("res://scenes/enemies/enemy.tscn")
	purpleEnemy = preload("res://scenes/enemies/enemy_type2.tscn")

func _physics_process(delta):
	if self.bossBattleStart:
		move1()
		middleShot()
	look_at(player.position)

func initiateCooldown():
	yield(get_tree().create_timer(2),"timeout")
	self.canFireMiddle = true

func middleShot():
	if self.canFireMiddle:
		self.canFireMiddle = false
		var bulletInstance = bullet.instance()
		bulletInstance.position = $middleBullet.get_global_position()
		bulletInstance.rotation_degrees = self.rotation_degrees
		bulletInstance.apply_impulse(Vector2(), Vector2(bulletSpeed, 0).rotated(self.rotation))
		get_tree().get_root().add_child(bulletInstance)
		yield(get_tree().create_timer(0.5),"timeout")
		self.canFireMiddle = true

func move1():
	if player != null:
		look_at(player.position)
		self.position += ((player.position - self.position) / 150)
		move_and_collide(self.velocity)

func receiveDamage(damage):
	hitPoint -= damage
	if hitPoint <= 0:
		$deathSFX.play()
		yield(get_tree().create_timer(0.1),"timeout")
		$deathSFX.play()
		yield(get_tree().create_timer(0.1),"timeout")
		$deathSFX.play()
		yield(get_tree().create_timer(0.1),"timeout")
		self.queue_free()

func _on_bodyDamage_body_entered(body):
	if body.is_in_group("player"):
		body.receiveDamage(self.bodyDamage)

func _on_greenSpawnerTimer_timeout():
	var instance = greenEnemy.instance()
	var instance2 = greenEnemy.instance()
	instance.speed = 50
	instance2.speed = 50
	var spawnPoint = get_node("greenSpawner")
	var spawnPoint2 = get_node("greenSpawner2")
	var num = get_tree().get_nodes_in_group("greenEnemy").size()
	if spawnPoint != null and num <= 7:
		instance.position = spawnPoint.get_global_position()
		instance2.position = spawnPoint2.get_global_position()
		get_node("../.").add_child(instance)
		get_node("../.").add_child(instance2)


func _on_purpleSpawnerTimer_timeout():
	var instance = purpleEnemy.instance()
	var instance2 = purpleEnemy.instance()
	instance.speed = 30
	instance2.speed = 30
	var spawnPoint = get_node("purpleSpawner")
	var spawnPoint2 = get_node("purpleSpawner2")
	var num = get_tree().get_nodes_in_group("purpleEnemy").size()
	if spawnPoint != null and num <= 4:
		instance.position = spawnPoint.get_global_position()
		instance2.position = spawnPoint2.get_global_position()
		get_node("../.").add_child(instance)
		get_node("../.").add_child(instance2)
