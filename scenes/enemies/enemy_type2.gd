extends KinematicBody2D

export (int) var hitPoints = 7

var damage = 1
var speed = 60

var velocity = Vector2()
var player

func _ready():
	player = get_parent().get_node("player")

func _physics_process(delta):
	if player != null:
		look_at(player.position)
		self.position += (player.position - self.position)/self.speed
		move_and_collide(self.velocity)

func receiveDamage(damage):
	self.hitPoints -= damage
	if self.hitPoints <= 0:
		$deathSound.play()
		yield(get_tree().create_timer(0.1),"timeout")
		queue_free()

func _on_body_area_body_entered(body):
	if body.is_in_group("player"):
		body.receiveDamage(self.damage)

func _on_silenceArea_body_entered(body):
	if body.is_in_group("player"):
		body.speed = 200
		body.isSilenced = true

func _on_silenceArea_body_exited(body):
	if body.is_in_group("player"):
		body.speed = 400
		body.isSilenced = false
