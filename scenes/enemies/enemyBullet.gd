extends RigidBody2D

var damage = 1
var speed = 600

var enemyBulletImpact

func _ready():
	enemyBulletImpact = preload("res://scenes/enemies/enemyBulletImpact.tscn")

func _on_DamageArea_body_entered(body):
	if body.is_in_group("enemy") or body.is_in_group("playerBullet"):
		pass
	elif body.is_in_group("player"):
		body.receiveDamage(self.damage)
		impactAnimation()
		queue_free()
	else:
		impactAnimation()
		queue_free()

func impactAnimation():
	var impactInstance = enemyBulletImpact.instance()
	impactInstance.position = get_global_position()
	impactInstance.rotation_degrees = rotation_degrees
	get_parent().add_child(impactInstance)
