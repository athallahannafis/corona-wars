extends ReferenceRect

var nextLevel

func _ready():
	DepartureBgm_Node.get_node(".").position = $Node2D.position
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	nextLevel = SceneSwitcher.getData("nextLevel")
	print(nextLevel)

func _process(delta):
	yield(get_tree().create_timer(2),"timeout")
	get_tree().change_scene(nextLevel)
