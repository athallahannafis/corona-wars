extends ColorRect

var m = [
	"You did it",
	"You have proven that corona can be defeated",
	"The world is safe",
	"For now..."
]

# Called when the node enters the scene tree for the first time.
func _ready():
	var msg = get_node("message")
	BossFightBgm_Node.get_node("bgm").stop()
	for index in range(m.size()):
		msg.text = m[index]
		yield(get_tree().create_timer(3),"timeout")
	get_tree().change_scene("res://scenes/menu/MainMenu.tscn")
