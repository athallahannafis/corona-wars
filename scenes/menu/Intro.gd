extends ColorRect

var message = [
	"This world is having a struggle..",
	"Due the global pandemic...",
	"COVID-19 everywhere...",
	"You are created to end this pandemic...",
	"A test must be done, a vaccine must be made",
	"If this test succeeded",
	"The world will be saved",
	"Now.. Proof yourself!"
]

func _ready():
	MainMenuBgm_Node.get_node("MainMenuBGM").stop()
	DepartureBgm_Node.get_node(".").get_node("AudioStreamPlayer2D").play()
	DepartureBgm_Node.get_node(".").position = $Node2D.position
	var msg = get_node("Message")
	
	for index in range(message.size()):
		msg.text = message[index]
		yield(get_tree().create_timer(3),"timeout")
	get_tree().change_scene("res://scenes/level/level1_arena.tscn")



func _on_SkipButton_pressed():
	get_tree().change_scene("res://scenes/level/level1_arena.tscn")
