extends ColorRect

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	BossFightBgm_Node.get_node("bgm").stop()
	DepartureBgm_Node.get_node("AudioStreamPlayer2D").stop()
	IntroBossBgm_Node.get_node("bgm").stop()
	MainMenuBgm_Node.get_node("MainMenuBGM").position = $BGMPosition.position
	MainMenuBgm_Node.get_node("MainMenuBGM").play()

func _on_StartGame_pressed():
	get_tree().change_scene("res://scenes/menu/Intro.tscn")


func _on_HowToPlay_pressed():
	get_tree().change_scene("res://scenes/menu/howToPlay.tscn")
